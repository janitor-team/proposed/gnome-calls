# Brazilian Portuguese translation for calls.
# Copyright (C) 2022 calls's COPYRIGHT HOLDER
# This file is distributed under the same license as the calls package.
# Bruno Lopes <brunolopesdsilv@gmail.com>, 2021.
# Rafael Fontenelle <rafaelff@gnome.org>, 2020-2021.
# Matheus Barbosa <mdpb.matheus@gmail.com>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: calls master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/calls/-/issues\n"
"POT-Creation-Date: 2022-01-18 02:44+0000\n"
"PO-Revision-Date: 2022-01-24 19:35-0300\n"
"Last-Translator: Matheus Barbosa <mdpb.matheus@gmail.com>\n"
"Language-Team: Brazilian Portuguese <gnome-pt_br-list@gnome.org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1)\n"
"X-Generator: Gtranslator 40.0\n"

#: data/org.gnome.Calls.desktop.in:3 src/calls-application.c:383
#: src/ui/call-window.ui:9 src/ui/main-window.ui:7
msgid "Calls"
msgstr "Chamadas"

#: data/org.gnome.Calls.desktop.in:4 data/org.gnome.Calls-daemon.desktop.in:4
msgid "Phone"
msgstr "Telefone"

#: data/org.gnome.Calls.desktop.in:5
msgid "A phone dialer and call handler"
msgstr "Discador de telefone e identificador de chamadas"

#. Translators: These are desktop search terms. Do not translate semicolons, end line with a semicolon.
#: data/org.gnome.Calls.desktop.in:7 data/org.gnome.Calls-daemon.desktop.in:7
msgid "Telephone;Call;Phone;Dial;Dialer;PSTN;"
msgstr "Telefone;Chamada;Fone;Discagem;Discador;RPTC;"

#: data/org.gnome.Calls-daemon.desktop.in:3
msgid "Calls (daemon)"
msgstr "Chamadas (daemon)"

# Não conheço o correspondente em português para daemon
#: data/org.gnome.Calls-daemon.desktop.in:5
msgid "A phone dialer and call handler (daemon mode)"
msgstr "Discador de telefone e identificador de chamadas (modo daemon)"

#: plugins/sip/sip-account-widget.ui:11
msgid "Add Account"
msgstr "Adicionar conta"

#: plugins/sip/sip-account-widget.ui:17
msgid "_Log In"
msgstr "_Conectar"

#: plugins/sip/sip-account-widget.ui:42
msgid "Manage Account"
msgstr "Gerenciar conta"

#: plugins/sip/sip-account-widget.ui:47
msgid "_Apply"
msgstr "_Aplicar"

#: plugins/sip/sip-account-widget.ui:61
msgid "_Delete"
msgstr "_Excluir"

#: plugins/sip/sip-account-widget.ui:91
msgid "Server"
msgstr "Servidor"

#: plugins/sip/sip-account-widget.ui:109
msgid "Display Name"
msgstr "Nome de exibição"

#: plugins/sip/sip-account-widget.ui:110
msgid "Optional"
msgstr "Opcional"

#: plugins/sip/sip-account-widget.ui:128
msgid "User ID"
msgstr "ID do usuário"

#: plugins/sip/sip-account-widget.ui:141
msgid "Password"
msgstr "Senha"

#: plugins/sip/sip-account-widget.ui:161
msgid "Port"
msgstr "Porta"

#: plugins/sip/sip-account-widget.ui:177
msgid "Transport"
msgstr "Transporte"

#: plugins/sip/sip-account-widget.ui:189
msgid "Use for Phone Calls"
msgstr "Usar para ligações telefônicas"

#: src/calls-application.c:548
#, c-format
msgid "Tried dialing invalid tel URI `%s'"
msgstr "Tentou discar para um telefone URI “%s” inválido"

#: src/calls-application.c:625
#, c-format
msgid "Don't know how to open `%s'"
msgstr "Não foi possível abrir “%s”"

#: src/calls-application.c:679
msgid "The name of the plugin to use as a call provider"
msgstr "O nome do plugin para usar como o provedor de chamada"

#: src/calls-application.c:680
msgid "PLUGIN"
msgstr "PLUGIN"

#: src/calls-application.c:685
msgid "Whether to present the main window on startup"
msgstr "Se deve-se apresentar a janela principal na inicialização"

#: src/calls-application.c:691
msgid "Dial a telephone number"
msgstr "Disca um número de telefone"

#: src/calls-application.c:692
msgid "NUMBER"
msgstr "NÚMERO"

#: src/calls-application.c:697
msgid "Enable verbose debug messages"
msgstr "Habilita mensagens de depuração detalhadas"

#: src/calls-application.c:703
msgid "Print current version"
msgstr "Exibe a versão atual"

# Troquei o sujeito pela indetificação (o número) porque é mais inteligível do que "chamador desconhecido" ou "pessoa desconhecida"
#: src/calls-best-match.c:357
msgid "Anonymous caller"
msgstr "Número desconhecido"

#: src/calls-call-record-row.c:110
#, c-format
msgid ""
"%s\n"
"yesterday"
msgstr ""
"%s\n"
"ontem"

#: src/calls-main-window.c:124
msgid "translator-credits"
msgstr ""
"Rafael Fontenelle <rafaelff@gnome.org>\n"
"Bruno Lopes <brunolopesdsilv@gmail.com>\n"
"Matheus Barbosa <mdpb.matheus@gmail.com>"

#: src/calls-main-window.c:322
msgid "Can't place calls: No voice-capable modem available"
msgstr ""
"Não é possível fazer chamadas: Nenhum modem com capacidade de voz disponível"

#: src/calls-main-window.c:326
msgid "Can't place calls: No modem or VoIP account available"
msgstr "Não é possível fazer chamadas: Nenhum modem ou conta VoIP disponível"

#: src/calls-main-window.c:331
msgid "Can't place calls: No backend service"
msgstr "Não é possível fazer chamadas: Nenhuma serviço de backend"

#: src/calls-main-window.c:335
msgid "Can't place calls: No plugin"
msgstr "Não é possível fazer chamadas: Nenhum plugin"

#: src/calls-main-window.c:375
msgid "Contacts"
msgstr "Contatos"

#: src/calls-main-window.c:385
msgid "Dial Pad"
msgstr "Teclado de discagem"

#. Recent as in "Recent calls" (the call history)
#: src/calls-main-window.c:394
msgid "Recent"
msgstr "Recentes"

#: src/calls-notifier.c:48
msgid "Missed call"
msgstr "Chamada perdida"

#. %s is a name here
#: src/calls-notifier.c:69
#, c-format
msgid "Missed call from <b>%s</b>"
msgstr "Chamada perdida de <b>%s</b>"

#. %s is a id here
#: src/calls-notifier.c:72
#, c-format
msgid "Missed call from %s"
msgstr "Chamada perdida de %s"

#: src/calls-notifier.c:74
msgid "Missed call from unknown caller"
msgstr "Chamada perdida de número desconhecido"

#: src/calls-notifier.c:80
msgid "Call back"
msgstr "Chamar de volta"

#: src/ui/account-overview.ui:16
msgid "VoIP Accounts"
msgstr "Contas VoIP"

#: src/ui/account-overview.ui:49
msgid "Add VoIP Accounts"
msgstr "Adiciona contas VoIP"

#: src/ui/account-overview.ui:51
msgid ""
"You can add VoIP account here. It will allow you to place and receive VoIP "
"calls using the SIP protocol. This feature is still relatively new and not "
"yet feature complete (i.e. no encrypted media)."
msgstr ""
"Você pode adicionar uma conta VoIP aqui. Isso vai permitir que você faça e "
"receba chamadas VoIP usando um protocolo SIP. Esse recurso ainda é "
"relativamente novo e ainda não está completo (por exemplo, sem mídia "
"criptografada)."

#: src/ui/account-overview.ui:58 src/ui/account-overview.ui:97
msgid "_Add Account"
msgstr "_Adicionar conta"

#. Translators: This is a verb, not a noun. Call the number of the currently selected row.
#: src/ui/call-record-row.ui:63
msgid "Call"
msgstr "Chamada"

#: src/ui/call-record-row.ui:103
msgid "_Delete Call"
msgstr "Apagar chama_da"

#. Translators: This is a phone number
#: src/ui/call-record-row.ui:108
msgid "_Copy number"
msgstr "_Copiar número"

#: src/ui/call-record-row.ui:113
msgid "_Add contact"
msgstr "_Adicionar contato"

#: src/ui/call-selector-item.ui:38
msgid "On hold"
msgstr "Em espera"

#: src/ui/contacts-box.ui:60
msgid "No Contacts Found"
msgstr "Nenhum contato encontrado"

#: src/ui/history-box.ui:10
msgid "No Recent Calls"
msgstr "Nenhuma chamada recente"

#: src/ui/main-window.ui:105
msgid "USSD"
msgstr "USSD"

#: src/ui/main-window.ui:114
msgid "_Cancel"
msgstr "C_ancelar"

#: src/ui/main-window.ui:131
msgid "_Close"
msgstr "Fe_char"

#: src/ui/main-window.ui:141
msgid "_Send"
msgstr "_Enviar"

#: src/ui/main-window.ui:214
msgid "_VoIP Accounts"
msgstr "Contas _VoIP"

#: src/ui/main-window.ui:227
msgid "_Keyboard shortcuts"
msgstr "Atalhos de _teclado"

#: src/ui/main-window.ui:233
msgid "_Help"
msgstr "Aj_uda"

#: src/ui/main-window.ui:239
msgid "_About Calls"
msgstr "_Sobre o Chamadas"

#: src/ui/new-call-box.ui:45
msgid "Enter a VoIP address"
msgstr "Insira um endereço VoIP"

#: src/ui/new-call-box.ui:58
msgid "Enter a number"
msgstr "Insira um número"

#: src/ui/new-call-box.ui:97
msgid "Dial"
msgstr "Discar"

#: src/ui/new-call-box.ui:120
msgid "Delete character in front of cursor"
msgstr "Excluir caractere na frente do cursor"

#: src/ui/new-call-box.ui:149
msgid "SIP Account"
msgstr "Contas SIP"

#: src/ui/new-call-header-bar.ui:6
msgid "New Call"
msgstr "Nova chamada"

#: src/ui/new-call-header-bar.ui:19
msgid "Back"
msgstr "Voltar"

#~ msgid "Dial a number"
#~ msgstr "Disca um número"

#~ msgid "Calling…"
#~ msgstr "Chamando…"

#~ msgid "Incoming phone call"
#~ msgstr "Chamada telefônica recebida"

#~ msgid "Mute"
#~ msgstr "Mudo"

#~ msgid "Speaker"
#~ msgstr "Alto-falante"

#~ msgid "Add call"
#~ msgstr "Adicionar chamada"

#~ msgid "Hold"
#~ msgstr "Colocar em espera"

#~ msgid "Hang up"
#~ msgstr "Desligar"

#~ msgid "Answer"
#~ msgstr "Atender"

#~ msgid "Hide the dial pad"
#~ msgstr "Ocultar o teclado numérico"

#~ msgid "Call the party"
#~ msgstr "Chamar a pessoa"

#~ msgid "+441234567890"
#~ msgstr "+551234567890"

#~ msgid "This call is not encrypted"
#~ msgstr "Esta chamada não está criptografada"

#~ msgid "This call is encrypted"
#~ msgstr "Esta chamada está criptografada"

#~ msgid "Recent Calls"
#~ msgstr "Chamadas recentes"

#~ msgid "New call…"
#~ msgstr "Nova chamada…"

#~ msgid "Menu"
#~ msgstr "Menu"

#~ msgid "About Calls"
#~ msgstr "Sobre o Chamadas"

#~ msgid "No modem found"
#~ msgstr "Nenhum modem encontrado"

#~ msgid "Backspace through number"
#~ msgstr "Apagar o número"

#~ msgid "Can't place calls: No SIM card"
#~ msgstr "Não é possível fazer chamadas: Nenhum cartão SIM"

#~ msgid "Incoming call"
#~ msgstr "Chamada recebida"

#~ msgid "View"
#~ msgstr "Visão"

#~ msgid "The CallsBestMatchView to monitor"
#~ msgstr "O CallsBestMatchView para monitorar"

#~ msgid "Name"
#~ msgstr "Nome"

#~ msgid "The display name of the best match"
#~ msgstr "O nome exibido da melhor correspondência"

#~ msgid "Inbound"
#~ msgstr "Recebida"

#~ msgid "Whether the call is inbound"
#~ msgstr "Se a chamada foi recebida"

#~ msgid "Number"
#~ msgstr "Número"

#~ msgid "The number the call is connected to if known"
#~ msgstr "O número ao qual a chamada está conectada, se conhecido"

#~ msgid ""
#~ "The name of the party the call is connected to, if the network provides it"
#~ msgstr ""
#~ "O nome da pessoa à qual a chamada está conectada, se a rede fornecer"

#~ msgid "State"
#~ msgstr "Estado"

#~ msgid "The current state of the call"
#~ msgstr "O estado atual da chamada"

#~ msgid "The call"
#~ msgstr "A chamada"

#~ msgid "Party"
#~ msgstr "Pessoa"

#~ msgid "The party participating in the call"
#~ msgstr "A pessoa participando na chamada"

#~ msgid "Data for the call this display will be associated with"
#~ msgstr "Dados para a chamada com a qual esta exibição estará associdada"

#~ msgid "The call to hold"
#~ msgstr "A chamada a colocar em espera"

#~ msgid "ID"
#~ msgstr "ID"

#~ msgid "The row ID"
#~ msgstr "O ID da linha"

#~ msgid "Target"
#~ msgstr "Destino"

# PTSN é rede de telefonia pública
#~ msgid "The PTSN phone number or other address of the call"
#~ msgstr "O número telefônica da RPTC ou outro endereço da chamada"

#~ msgid "Whether the call was an inbound call"
#~ msgstr "Se a chamada era uma chamada recebida"

#~ msgid "Start"
#~ msgstr "Início"

#~ msgid "Time stamp of the start of the call"
#~ msgstr "Registro de data e hora do início da chamada"

#~ msgid "Answered"
#~ msgstr "Atendida"

#~ msgid "Time stamp of when the call was answered"
#~ msgstr "Registro de data e hora de quanto a chamada foi atendida"

#~ msgid "End"
#~ msgstr "Fim"

#~ msgid "Time stamp of the end of the call"
#~ msgstr "Registro de data e hora do fim da chamada"

#~ msgid "Record"
#~ msgstr "Registro"

#~ msgid "The call record for this row"
#~ msgstr "A registro de chamadas para esta linha"

#~ msgid "Interface for libfolks"
#~ msgstr "Interface para libfolks"

#~ msgid "Encrypted"
#~ msgstr "Criptografada"

#~ msgid "model"
#~ msgstr "modelo"

#~ msgid "The data store containing call records"
#~ msgstr "O armazenamento de dados contendo registros de chamadas"

#~ msgid "Record store"
#~ msgstr "Armazenamento de registros"

#~ msgid "The store of call records"
#~ msgstr "O armazenamento dos registros de chamadas"

#~ msgid "The party's name"
#~ msgstr "O nome da pessoa"

#~ msgid "Status"
#~ msgstr "Status"

#~ msgid "A text string describing the status for display to the user"
#~ msgstr "Um texto que descreve o status para exibição para o usuário"
